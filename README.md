## Twig Hash

The Twig Hash module provides various hashing (and similar)
functions for template developers.

### Hash

The md5 of 123 is:

```twig
{{ 123 | hash }}
```

You can use any of the php hash algorithms by passing their 
name as a string.

The sha512 of 123 is:

```twig
{{ 123 | hash('sha512') }}
```

### Base convert

baseConv - convert a number to a different base. Defaults to 
decimal to hex.

123A hex in base36 is:

```twig
{{ "123A" | baseConv(16,36) }}
```

### Unique ID

uuid - return a unique id

Here's a unique id:

```twig
{{ uuid() }}
```

### More examples

With twig chaining...

Here's a unique id that's even shorter 

```twig
{{ uuid() | baseConv(16,36) }}
```

Top tip example - use to create unique class names in block or 
Display Suite templates.

```twig
{% set classId = "bg-" ~ (uuid() | baseConv(16,36)) %}
<style>
  @media all and (min-width: 768px) { .{{ classId  }} {background-color: red;} }
  @media all and (max-width: 767px) { .{{ classId  }} {background-color: blue;} }
</style>
<div class='{{ classId  }}'>Hello!</div>
```

#### Maintainers

- George Anderson (https://www.drupal.org/u/geoanders)
