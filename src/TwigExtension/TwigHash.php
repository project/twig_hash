<?php

namespace Drupal\twig_hash\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;


/**
 * Twig extension to hash an input string.
 */
class TwigHash extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array {
    return [
      new TwigFilter('hash', [$this, 'hashFilter']),
      new TwigFilter('baseConv', [$this, 'baseConvFilter']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('uuid', [$this, 'getUuid']),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   */
  public function getName(): string {
    return 'twig_hash.twig_extension';
  }

  /**
   * Returns the hashed version of a variable.
   *
   * @param string $string
   *   A string containing the text to be MD5'd.
   * @param string $hash_type
   *   The hash type. Defaults to md5.
   *
   * @return string
   *   An MD5'd version of the input string.
   */
  public static function hashFilter($string, $hash_type = "md5"): string {
    if (!in_array($hash_type, hash_algos())) {
      return "Invalid hash algorithm";
    }
    return hash($hash_type, $string);
  }

  /**
   * Returns a variable converted to an alternative base.
   *
   * @param string $string
   *   A string containing the text to be base converted.
   * @param int $from
   *   The base number is in.
   * @param int $to
   *   The base to convert number to.
   *
   * @return string
   *   A base converted version of the input string.
   */
  public static function baseConvFilter($string, $from = 10, $to = 16): string {
    return base_convert($string, $from, $to);
  }

  /**
   * Returns a unique id.
   *
   * @return string
   *   A unique id.
   */
  public static function getUuid(): string {
    return uniqid();
  }

}
